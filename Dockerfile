FROM node:lts-alpine

ENV NODE_ENV development


WORKDIR /usr/app
COPY package.json .
RUN npm install
COPY . .
RUN npm run build
CMD npm run start
