import { RedisPubSub } from 'graphql-redis-subscriptions'
import Redis from 'ioredis'
import { exit } from 'process'

const host = 'localhost:7001'
console.log('Redis host: ', host)

const pubRedis = new Redis(host).on('error', err => {
    console.log('Redis not swimming', err)
    exit(2)
})

const subRedis = new Redis(host).on('error', err => {
    console.log('Redis not swimming', err)
    exit(2)
})
export const pubSub = new RedisPubSub({
    publisher: pubRedis,
    subscriber: subRedis,
})
