import './loadenv'
import { ValidationPipe } from '@nestjs/common'
import { NestFactory } from '@nestjs/core'
import { AppModule } from './app.module'
import './kafka/kafka'

async function bootstrap() {
    const app = await NestFactory.create(AppModule)
    app.useGlobalPipes(new ValidationPipe())
    const port = process.env.PORT
    console.log(`Running on port ${port}`)
    await app.listen(port)
    console.log(`Application is running on: ${await app.getUrl()}`)
}
bootstrap()
