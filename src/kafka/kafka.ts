import { Kafka } from 'kafkajs'
import { Cat } from 'src/graphql.schema'
import { pubSub } from 'src/redis/redis'

run().then(
    () => console.log('Successfully connected to KAFKA'),
    err => console.log(err),
)

function* idGen() {
    let id = 100
    while (true) {
        yield id
        id = id + 1
    }
}
const idGenerator = idGen()

async function run() {
    const kafka = new Kafka({ brokers: ['localhost:9092'] })
    // If you specify the same group id and run this process multiple times, KafkaJS
    // won't get the events. That's because Kafka assumes that, if you specify a
    // group id, a consumer in that group id should only read each message at most once.
    const consumer = kafka.consumer({
        groupId: 'catsGroup' + Date.now(),
        allowAutoTopicCreation: true,
    })

    await consumer.connect()
    await consumer.subscribe({ topic: 'cats', fromBeginning: false })
    await consumer.run({
        eachMessage: async data => {
            const name = data.message.value.toString()
            console.log(name)
            pubSub.publish<{ catCreated: Cat }>('catCreated', {
                catCreated: {
                    id: idGenerator.next().value || 0,
                    age: 10,
                    name,
                },
            })
        },
    })
}
