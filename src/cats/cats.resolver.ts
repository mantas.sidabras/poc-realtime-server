import { ParseIntPipe, UseGuards } from '@nestjs/common'
import { Args, Mutation, Query, Resolver, Subscription } from '@nestjs/graphql'
import { filter } from 'rxjs'
import { pubSub } from 'src/redis/redis'
import { Cat } from '../graphql.schema'
import { CatsGuard } from './cats.guard'
import { CatsService } from './cats.service'
import { CreateCatDto } from './dto/create-cat.dto'

@Resolver('Cat')
export class CatsResolver {
    constructor(private readonly catsService: CatsService) {}

    @Query('cats')
    @UseGuards(CatsGuard)
    async getCats() {
        return this.catsService.findAll()
    }

    @Query('cat')
    async findOneById(
        @Args('id', ParseIntPipe)
        id: number,
    ): Promise<Cat> {
        return this.catsService.findOneById(id)
    }

    @Mutation('createCat')
    async create(@Args('createCatInput') args: CreateCatDto): Promise<Cat> {
        const createdCat = this.catsService.create(args)
        console.log('Created cat', createdCat)
        pubSub.publish('catCreated', { catCreated: createdCat })
        return createdCat
    }

    @Subscription(returns => Cat, {
        filter: (payload, variables) => {
            if (!variables.age) return true // if age is not provided, push all events
            return payload.catCreated.age === variables.age // if age is provided, push only matched events
        },
    })
    catCreated(@Args('age') age: Cat['age']) {
        return pubSub.asyncIterator('catCreated') // subscribe to catCreated GraphQL topic
    }
}
