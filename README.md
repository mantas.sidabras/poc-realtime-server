### Graphql Apollo sample

### Installation

make sure you have `docker` and `docker-compose` installed on your system. You can use mac docker desktop or Rancher for that.

Start Redis and Kafka services using `docker-compose up`
You can stop the services by hitting `Ctrl + c`
if kafka broker won't start, try dropping the containers using `docker container prune` and start again

Disclaimer: might take a minute for kafka to spinup

Run application in another shell

`npm install`
`npm run start:dev`

If everything is successful it should say that the app successfully connected to kafka.


### Graphql Playground

When the application is running, you can go to [http://localhost:4000/graphql](http://localhost:4000/graphql) to access the GraphQL Playground.  See [here](https://docs.nestjs.com/graphql/quick-start#playground) for more.

Open 2 tabs or 2 windows.

### Subscription

Start subscription in 1 tab
```
subscription OnCatCreated {
  catCreated {
    id
    name
    age
  }
}
```

or to listen for specific age cats:

```
subscription OnCatCreated {
  catCreated(age: 10) {
    id
    name
    age
  }
}
```

Execute mutation in another tab

```
mutation CreateCat {
  createCat (createCatInput:{
    name:"Fluffy",
    age: 10
  }) {
    id
    name
    age
  }
}
```

After executing mutation you should see newly created cat in Subscription tab 

See `cats.resolver.ts`
See `kafka.ts`

### Produce kafka messages

Use this command in yet another shell to enter interactive kafka topic
```
docker exec --interactive --tty broker \
kafka-console-producer --bootstrap-server broker:9092 \
                       --topic cats
```

Inside you can type any message, which will be your event content (Cat name)
exit using `Ctrl + c`

Once a message is produced, it should trigger the subscription (hardcoded age value = 10)

Once you're done, clean docker using `docker system prune`. Warning this will delete all your docker images and containers